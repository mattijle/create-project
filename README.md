### create-project

Simple tool to bootstrap create-react-app or gatsby apps.

Install:

`
  git clone git@gitlab.com:mattijle/create-project.git create-project
`

`  
  npm install -g ./create-project
`


Usage: 

`
  create-project <projectName> (<gitRepo>)
`
