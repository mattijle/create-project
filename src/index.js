#!/usr/bin/env node
const inquirer = require('inquirer');
const { spawn } = require('child_process');

const CREATE_REACT_APP = 'create-react-app';
const GATSBY = 'gatsby';

const PROJECT_TYPES = [CREATE_REACT_APP,GATSBY];

const createNewProject = ({projectType, projectName, projectRepo}) => {
  let spawnArgs = [];
  switch (projectType){
    case CREATE_REACT_APP:
      spawnArgs = [projectType, projectName]
      break;
    case GATSBY:
      spawnArgs = [projectType, 'new', projectName];
      break;
    default:
      return;
  }
  const newApp = spawn('npx',spawnArgs);
  newApp.stdout.on('data', (data) => {
    console.log(data.toString());
  });
  newApp.on('exit', (code)=> {
    if(code === 1){
      process.exit(1);
    }
    if(projectRepo !== undefined) {
      process.chdir(process.cwd()+'/'+projectName);
      spawn('git', ['init']).on('exit', (ex)=>{
        const git = spawn('git', ['remote', 'add', 'origin', projectRepo]);
        git.stdout.on('data', (data) => {
          console.log(data.toString());
        });
      });
    }
  });

}

(async () => {
  const projectName = process.argv[2];
  const projectRepo = process.argv[3];
  if(projectName === undefined) {
    console.warn('Usage: create-project <project_name> (git-repo)');
    process.exit(1);
  }

  const { projectType } = await inquirer.prompt([
    {
      type: 'list',
      message: 'Select the project type you want to bootstrap:',
      name: "projectType",
      choices: PROJECT_TYPES,
    }
  ]);
  createNewProject({projectType, projectName, projectRepo});
 })();
